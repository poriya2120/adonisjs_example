'use strict'

class PostController {
    async index({view}){
        const posts=[
            {name:'poriya',age:'20'},
            {name:'ali',age:'30'},
            {name:'taha',age:'14'},
            {name:'vahid',age:'40'}
        ]
        return view.render('posts.index',{
            title:'this is new post',
            posts:posts
        })
        // return 'posts'
    }
}

module.exports = PostController
